package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Comment(
    val id: String,
    val movieId: String,
    val comment: String,
    val date: String
)
