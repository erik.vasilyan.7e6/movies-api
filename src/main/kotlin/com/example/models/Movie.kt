package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Movie(
    val id: String,
    val title: String,
    val year: String,
    val genre: String,
    val director: String,
    val comments: MutableList<Comment>
)

val movieStorage = mutableListOf<Movie>()
