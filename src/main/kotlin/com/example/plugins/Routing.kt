package com.example.plugins

import com.example.routes.commentRouting
import com.example.routes.movieRouting
import io.ktor.server.routing.*
import io.ktor.server.response.*
import io.ktor.server.application.*

fun Application.configureRouting() {
    routing {
        movieRouting()
        commentRouting()
    }
}