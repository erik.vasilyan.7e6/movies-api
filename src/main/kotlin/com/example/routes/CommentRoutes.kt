package com.example.routes

import com.example.models.Comment
import com.example.models.movieStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.commentRouting() {
    route("/movies/{id?}") {

        post {
            val id = call.parameters["id"]
            for (movie in movieStorage) {
                if (movie.id == id) {
                    val comment = call.receive<Comment>()
                    movie.comments.add(comment)
                    call.respondText("Comment added in movie with id $id", status = HttpStatusCode.Accepted)
                    return@post
                }
            }
            call.respondText("No movie found with id $id", status = HttpStatusCode.NotFound)
        }

        route("/comments") {
            get {
                val id = call.parameters["id"]
                for (movie in movieStorage) {
                    if (movie.id == id) {
                        if (movie.comments.isEmpty()) call.respondText("No comments found in movie with id $id", status = HttpStatusCode.OK)
                        else call.respond(movie.comments)
                        return@get
                    }
                }
                call.respondText("No movie found with id $id", status = HttpStatusCode.NotFound)
            }
        }
    }
}