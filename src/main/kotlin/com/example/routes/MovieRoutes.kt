package com.example.routes

import com.example.models.Comment
import com.example.models.Movie
import com.example.models.movieStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.movieRouting() {
    route("/movies") {

        get {
            if (movieStorage.isNotEmpty()) {
                call.respond(movieStorage)
            } else {
                call.respondText("No movies found", status = HttpStatusCode.OK)
            }
        }

        get("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                "Missing movie id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (customer in movieStorage) {
                if (customer.id == id) return@get call.respond(customer)
            }
            call.respondText( "No movie found with id $id", status = HttpStatusCode.NotFound)
        }

        post {
            val movie = call.receive<Movie>()
            movieStorage.add(movie)
            call.respondText("Movie stored correctly", status = HttpStatusCode.Created)
        }

        put("{id?}") {
            val id = call.parameters["id"]
            val movie = call.receive<Movie>()
            for (i in movieStorage.indices) {
                if (movieStorage[i].id == id) {
                    movieStorage[i] = movie
                    return@put call.respondText("Movie updated correctly", status = HttpStatusCode.Accepted)
                }
            }
            call.respondText("No movie found with id $id", status = HttpStatusCode.NotFound)
        }


        delete("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@delete call.respondText(
                "Missing movie id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (customer in movieStorage) {
                if (movieStorage.removeIf { it.id == id }) {
                    call.respondText("Movie removed correctly", status = HttpStatusCode.Accepted)
                } else {
                    call.respondText("Not Found", status = HttpStatusCode.NotFound)
                }
            }
        }
    }
}
